                                                       Projet Rust 


Je voudrais faire un jeux vidéo. En 2d ou en 3d, même si pour l’instant un jeu vidéo en 2d me parait déjà assez compliqué. Je partirais plus sur un jeu 2d de plateforme à la mario, pas vraiment original mais c’est pas grave. Faut bien commencer quelque pars.

Je suis seul. J’ai envie de le faire seul, je veux pouvoir concevoir chaque aspect du jeux, ça va être très enrichissant.

Pour faire un jeu vidéo vous avez envoyé différentes sources, je pense utiliser bevy, j’ai vu très brièvement ce que c’était, ça me semble ludique et cool.

Je ne me suis pas encore bien renseigné sur les différentes étapes d’un tel projet. Les étapes qui me semble évidentes sont  : (pas forcément dans l’ordre)
-Création du monde, l’environnement où évoluera le personnage.
-Création du personnage
-Interaction du personnage avec le monde
-Ennemi/obstacle au joueur
-Objectif à atteindre pour gagner
-Création des skins du personnage, des ennemis, du monde, (animation ? peut être trop compliqué pour faire un rendu beau ? ou alors animé sur 2 trames)
-Ajouter des musique, du sons lors des interactions avec les ennemie et le monde.
-Pouvoir sauvegarder une partie, et en charger.

Et des étapes secondaires :
-Ajouter des points 
-Une monnaie 
-Des vies pour le personnage 
-Une barre de vie 
-Des armes
-Des boss
-Plusieur map.

J’avoue que je ne me rends pas bien compte du travail que ça demande. Je rectifierai surement des choses pour que ça devienne faisable.

****************************************************************************************************


MISE À JOUR 28/12/2020

Push du projet godot engine en lui même, pas vraiment fini(80%).
Les décors et la durée de vie du jeu sont à améliorer. Question décors, il y a du paint(bof), et le jeux dure 1 à 2 minutes.
Decors en Pixel art prévu(24h de travail ), avec une durée de vie d'au moins 10 minutes(5h de travail).

Manque l'implémentation complète du script godot en rust (0%)(Beaucoup de travail..).

****************************************************************************************************


MISE À JOUR 10/01/2021

Projet godot engine en rust terminé.

Il manque certaine implémentation que je n'arrive pas à faire en rust...
Comme par exemple le fait que mon personnage ne puisse tirer que dans un seul sens... Mais sinon tout marche bien.

Sources et aides pour la réalisation de ce projet :

Rust :
https://docs.rs/gdnative/0.9.1/gdnative/


https://paytonrules.com/post/games-in-rust-with-godot-part-one/


https://github.com/godot-rust/godot-rust


https://godot-rust.github.io/


Godot :

https://docs.godotengine.org/fr/stable/classes/index.html


****************************************************************************************************


MISE À JOUR 12/01/2021

Ajout de l'executable. J'ai oublié de le mettre en re-regardant mon git..

Pour executer le jeu, allez dans ./ProjetGodotRust et taper ./Projet\ Godot.x86_64


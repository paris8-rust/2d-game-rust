use crate::extensions::NodeExt;
use gdnative::api::{KinematicBody2D, AnimatedSprite, Node2D};
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(KinematicBody2D)]
#[user_data(user_data::MutexData<FlyingMob>)]

/// Structure de la classe MobFlying, Avec une référence sur la classe Mob.
pub struct FlyingMob {

    #[property(default = 200.0)]
    pub speed: f32,
    velocity: Vector2,
    target: Vector2,
    #[property(default = false)]
    player: bool,

}

/// Implémentation de la classe MobFlying.
#[methods]
impl FlyingMob {

    /// On crée une nouvelle instance en initialisant toutes les variable.
    fn new(_owner: &KinematicBody2D) -> Self {

        FlyingMob {
            speed: 400.0,
            velocity: Vector2::new(0.0, 0.0),
            target: Vector2::new(0.0, 0.0),
            player: false,
        }
    }

    /// Fonction qui est appellée une seule fois, quand le body2D est instancié.
    /// On joue l'animation fly du MobFlying.
    #[export]
    fn _ready(&mut self, owner: &KinematicBody2D){

        let flyingmob_sprite = unsafe { owner.get_typed_node::<AnimatedSprite, _>("AnimatedSprite") };
        self.player = false;

        let animation = "fly";
        flyingmob_sprite.play(animation, false);

    }

    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup),
    ///  et qui peut utiliser des fonctions qui simulent des évenments physiques.
    /// Ici on test si la player est vrai (que le player est entré dans sa CollisionShape),
    /// si c'est le cas le MobFlying va se déplacer vers les coordonnée du joueur où il est entrée dans la CollisionShape
    #[export]
    fn _physics_process(&mut self, owner: &KinematicBody2D,delta: f32){
        
        if self.player == true {

            self.velocity = owner.position().direction_to(self.target) * self.speed;
            
        }
        owner.move_and_slide(self.velocity,Vector2::new(0.0, -1.0),true,2000,1.0,true);

    }

    /// Cette fonction détecte les body2D qui entrent dans sa CollisionShape,
    /// On test si le body est dans le group "player",
    /// si oui on prend ses coordonnée global et on passe la viriable player à true.
    /// Pour dire que le player est entré dans sa collisionShape.
    /// Chaque MobFlying a 3 CollisionShape car,
    /// si le player entre dans sa deuxieme ou troisieme collision shape,
    /// le MobFlying donnera l'impression de le suivre.
    #[export]
    fn _on_Detection_body_entered(&mut self,owner: &KinematicBody2D,_body: Variant ){
        
        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()
        };


        if _body.is_in_group("player"){

            self.target = _body.global_position();
            self.player = true;
        }

    }

    /// Cette fonction détecte les body2D qui sortent de sa CollisionShape,
    /// On test si le body est dans le group "player",
    /// On passe la variable player à false.
    /// Pour dire qu'il n'est plus dans sa CollisionShape.
    #[export]
    fn _on_Detection_body_exited(&mut self,owner: &KinematicBody2D,_body: Variant ){
        
        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()
        };

        if _body.is_in_group("player"){

            self.player = false;
        }

    }
}

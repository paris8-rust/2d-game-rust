/// Fonction entierement prise d'un des exemples de la doc godot rust,
/// ce fichier sert à prendre plus simplement les nodes de Godot Engine.

use gdnative::prelude::*;

pub trait NodeExt {

    unsafe fn get_typed_node<T, P>(&self, path: P) -> TRef<'_, T, Shared>
    where
        T: GodotObject + SubClass<Node>,
        P: Into<NodePath>;
}

impl NodeExt for Node {
    unsafe fn get_typed_node<T, P>(&self, path: P) -> TRef<'_, T, Shared>
    where
        T: GodotObject + SubClass<Node>,
        P: Into<NodePath>,
    {
        self.get_node(path.into())
            .expect("node should exist")
            .assume_safe()
            .cast()
            .expect("node should be of the correct type")
    }
}
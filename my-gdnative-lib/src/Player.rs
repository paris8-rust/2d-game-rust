use crate::extensions::NodeExt;
use gdnative::api::{KinematicBody2D, AnimatedSprite,Node, CollisionShape2D};
use gdnative::prelude::*;
use gdnative::*;
use crate::BulletFire;
use crate::BulletIce;

#[derive(NativeClass)]
#[inherit(KinematicBody2D)]
#[user_data(user_data::MutexData<Player>)]
#[register_with(Self::register_Player)]

/// Structure de la classe MobFlying, Avec une référence sur la BulletIce et BulletFire
pub struct Player {
    #[property]
    BulletFire: Option<Ref<PackedScene,Shared>>,
    #[property]
    BulletIce: Option<Ref<PackedScene,Shared>>,
    #[property(default = 2000.0)]
    pub GRAVITY: f32,
    #[property(default = 500.0)]
    pub WALK_SPEED: f32,
    #[property(default = 700.0)]
    pub JUMP_SPEED: f32,
    #[property(default = false)]
    pub en_vie: bool,
    #[property(default = 3)]
    pub vie: i32,
    dire: Vector2,
    velocity: Vector2,
    nbsautenlair: i32,
    bouc: i32,
    arme: i32,

}

#[methods]
impl Player {

    /// Fonction permetant de gerer le signal "dead" qui sera utiliser dans la scene HUD
    fn register_Player(builder: &ClassBuilder<Self>){
        builder.add_signal(Signal {
            name: "dead",
            args: &[],

        });
    }

    /// On crée une nouvelle instance en initialisant toutes les variables.
    fn new(_owner: &KinematicBody2D) -> Self {

        Player {
            BulletFire: None,
            BulletIce: None,
            GRAVITY:2000.0,
            WALK_SPEED:700.0,
            JUMP_SPEED:700.0,
            en_vie: true,
            vie: 1,
            velocity:  Vector2::new(0.0, 0.0),
            dire: Vector2::new(0.0, 0.0),
            nbsautenlair: 0,
            bouc: 0,
            arme: 1,
        }
    }

    /// Fonction qui est appellée une seule fois, quand le body2D est instancié.
    /// On importe les scene BulletIce et BulletFire.
    #[export]
    fn _ready(&mut self, owner: &KinematicBody2D){

        let coeur1 = unsafe { owner.get_typed_node::<Sprite, _>("coeur") };
        let coeur2 = unsafe { owner.get_typed_node::<Sprite, _>("coeur2") };
        let coeur3 = unsafe { owner.get_typed_node::<Sprite, _>("coeur3") };
        let bouc_argent = unsafe { owner.get_typed_node::<Sprite, _>("BoucArgent") };
        let bouc_or = unsafe { owner.get_typed_node::<Sprite, _>("BoucOr") };

        coeur1.show();
        coeur2.show();
        coeur3.show();
        bouc_argent.hide();
        bouc_or.hide();

        let resource_loader = ResourceLoader::godot_singleton();

        self.BulletFire = resource_loader
            .load("res://BulletFire.tscn", "PackedScene", false)
            .and_then(|res| res.cast::<PackedScene>());
        
        self.BulletIce = resource_loader
            .load("res://BulletIce.tscn", "PackedScene", false)
            .and_then(|res| res.cast::<PackedScene>());
    }
    
    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup)
    /// Dans cette fonction on va gerer les tir du player, et l'arme qu'il choisit.
    /// Si le joueur est en vie.
    /// Il peut choisir son arme : Ice : arme 1(touche &) Fire : arme 2(touche é),
    /// et tirer avec "shoot" qui représente le clic gauche
    /// Si on appuie sur & ou é on selectionne l'arme avec laquelle on veut tirer,
    /// on va passer à la variable arme les variables 1 ou 2.
    /// Si on clic gauche, avec arme == 1, on va tirer un projectile de glace,
    /// Si on clic gauche, avec arme == 2 et que le temps de d'attente est fini, on va tirer un projectile de feu
    /// 
    /// 
    /// (Je ne suis pas arrivé à faire en sorte que le player puisse tirer dans le sens où il regarde, là il ne tirera qu'a droite...)
    #[export]
    fn _process(&mut self, owner: &KinematicBody2D, delta: f32){
        let timer_fire_bullet = unsafe { owner.get_typed_node::<Timer, _>("TimerFireBullet") };

        let fire= unsafe { owner.get_typed_node::<Sprite, _>("Fire") };

        if timer_fire_bullet.time_left() == 0.0 {
            fire.hide();
        }

        let input = Input::godot_singleton();

        if self.en_vie == true {
            if Input::is_action_just_pressed(&input, "shoot") && self.arme == 1 {

                if let Some(bullet_scene) = self.BulletIce.take() {
                    let bullet_scene = unsafe { bullet_scene.assume_safe() };
                    let bullet = bullet_scene
                        .instance(PackedScene::GEN_EDIT_STATE_DISABLED)
                        .and_then(|b| {
                            let b = unsafe { b.assume_unique() };
                            b.cast::<KinematicBody2D>()
                        })
                        .expect("Could not intantiate bullet scene!");

                    bullet.set_global_position(Vector2::new(owner.global_position().x,owner.global_position().y));

                    if let Some(root_scene) = owner.get_node("/root/Main"){
                        let root_scene = unsafe {root_scene.assume_safe()};
                        root_scene.add_child(bullet,false);
                    }

                    self.BulletIce.replace(bullet_scene.claim());
                }


            }
            if Input::is_action_just_pressed(&input, "shoot") && self.arme == 2 && timer_fire_bullet.time_left() == 0.0 {

                timer_fire_bullet.start(0.0);
                fire.show();
                if let Some(bullet_scene) = self.BulletFire.take() {
                    let bullet_scene = unsafe { bullet_scene.assume_safe() };
                    let bullet = bullet_scene
                        .instance(PackedScene::GEN_EDIT_STATE_DISABLED)
                        .and_then(|b| {
                            let b = unsafe { b.assume_unique() };
                            b.cast::<KinematicBody2D>()
                        })
                        .expect("Could not intantiate bullet scene!");

                    bullet.set_global_position(Vector2::new(owner.global_position().x,owner.global_position().y));

                    if let Some(root_scene) = owner.get_node("/root/Main"){
                        let root_scene = unsafe {root_scene.assume_safe()};
                        root_scene.add_child(bullet,false);
                    }

                    self.BulletFire.replace(bullet_scene.claim());


                }

            }
            if Input::is_action_just_pressed(&input, "Ice") {
                self.arme = 1;
            }
            if Input::is_action_just_pressed(&input, "Fire") {
                self.arme = 2;
            }
        }

    }

    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup),
    ///  et qui peut utiliser des fonctions qui simulent des évenments physiques.
    /// Ici on va gérer les déplacements du player,
    /// touche q on fixe la vélocité en x vers la gauche de WALK_SPEED
    /// touche d on fixe la vélocité en x vers la droite de WALK_SPEED
    /// touche z fixe la vélocité en y vers le haut de JUMP_SPEED,
    /// et si la variable sautenlair est égal à 0, on peut éffectué un double saut.
    /// Puis on va gerer l'affichafge des coeurs,
    /// 1 vie = Affiche 1 coeur, 2 vie = Affiche 2 coeurs, 3 vie = Affiche 3 coeurs.
    #[export]
    fn _physics_process(&mut self,owner: &KinematicBody2D,delta: f32){

        self.velocity.y = self.velocity.y + delta * self.GRAVITY;


        self.dire = owner.get_global_mouse_position() - owner.global_position();


        let mut player_animation = unsafe { owner.get_typed_node::<AnimatedSprite, _>("PlayerAnimation") };
        let animation;

        if owner.is_on_floor(){
            self.nbsautenlair = 0;
            self.velocity.y = self.GRAVITY/10.0;
        }

        let input = Input::godot_singleton();


        if Input::is_action_pressed(&input, "ui_left"){
            self.velocity.x =  -self.WALK_SPEED;
            animation = "walk";
            player_animation.set_flip_h(true);
            player_animation.play(animation,false); 
        }

        else if Input::is_action_pressed(&input, "ui_right"){
            self.velocity.x =  self.WALK_SPEED;
            animation = "walk";
            player_animation.set_flip_h(false);
            player_animation.play(animation,false); 
        }
        else {
            self.velocity.x = 0.0;
            let animation = "stopped";
            player_animation.play(animation,false); 
        }

        if owner.is_on_floor() && Input::is_action_pressed(&input, "ui_up"){
            self.velocity.y = -self.JUMP_SPEED;
        }

        if !owner.is_on_floor() {

            if Input::is_action_pressed(&input, "ui_left"){
                self.velocity.x = -self.WALK_SPEED/1.5;
            }
            else if Input::is_action_pressed(&input, "ui_right"){
                self.velocity.x = self.WALK_SPEED/1.5;
            }
            else {
                self.velocity.x = 0.0;
                player_animation.stop();
                let animation = "stopped";
                player_animation.play(animation,false); 
            }
        }

        if !owner.is_on_floor() && Input::is_action_just_pressed(&input, "ui_up") && self.nbsautenlair == 0 {
            self.nbsautenlair = 1;
            self.velocity.y =  -self.JUMP_SPEED;
        }

        let coeur1 = unsafe { owner.get_typed_node::<Sprite, _>("coeur") };
        let coeur2 = unsafe { owner.get_typed_node::<Sprite, _>("coeur2") };
        let coeur3 = unsafe { owner.get_typed_node::<Sprite, _>("coeur3") };

        if self.vie == 3 {
            coeur3.show();
        }
        if self.vie == 2 {
            coeur3.hide();
            coeur2.show();
        }
        if self.vie == 1 {
            coeur3.hide();
            coeur2.hide();
            
        }

        owner.move_and_slide(self.velocity,Vector2::new(0.0, -1.0),true,2000,1.0,true);

    }
    
    /// Cette fonction détecte les body2D qui entrent dans sa CollisionShape,
    /// On va tester si le body est dans l'un des groupes : mobs, boss, ItemCoeur, BoucArgent, BoucOr.
    /// Groupe mob ou boss ;
    /// Si il a 0 de bouc il perd 1 point de vie,
    /// Si il a 1 de bouc, il a 0 point de bouclier,
    /// Si il a 2 de bouc il a 1 point de bouclier,
    /// Si il a 0 point de vie, on émet le signal "dead".
    /// Groupe ItemCoeur :
    /// Si il a moins de 3 vie, ça lui ajoute 1 vie.
    /// Groupe BoucArgent :
    /// Il a 1 point de bouc,
    /// Groupe BoucOr
    /// Il a 2 points de bouc.
    #[export]
    fn _on_Area2D_body_entered(&mut self,owner: &KinematicBody2D,_body: Variant ){

        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()
        };

        let mut player_animation = unsafe { owner.get_typed_node::<AnimatedSprite, _>("PlayerAnimation") };

        let animation;
        
        if _body.is_in_group("mobs") || _body.is_in_group("boss") {

            let bouc_argent = unsafe { owner.get_typed_node::<Sprite, _>("BoucArgent") };
            let bouc_or = unsafe { owner.get_typed_node::<Sprite, _>("BoucOr") };
        
            if self.bouc == 0 {
                bouc_argent.hide();
                bouc_or.hide();
                self.vie = self.vie - 1;
            }
        
            else if self.bouc == 1 {
                self.bouc = 0;
                bouc_argent.hide();
            }
        
            else if self.bouc == 2 {
                bouc_argent.show();
                bouc_or.hide();
                self.bouc = 1;
            }

            animation = "hit";
            player_animation.play(animation,false);

            if self.vie == 0 {
                self.en_vie = false;
                owner.emit_signal("dead", &[]);
                owner.hide();

                let collision_shape = unsafe { owner.get_typed_node::<CollisionShape2D, _>("CollisionShape2D") };

                collision_shape.set_deferred("disabled", true);

                self.GRAVITY = 0.0;
                self.WALK_SPEED = 0.0;
                self.JUMP_SPEED = 0.0;
            }
        }

        let bouc_argent = unsafe { owner.get_typed_node::<Sprite, _>("BoucArgent") };
        let bouc_or = unsafe { owner.get_typed_node::<Sprite, _>("BoucOr") };
        

        if _body.is_in_group("ItemCoeur")
        {
            if self.vie < 3 {
                self.vie = self.vie + 1;
            }
            _body.queue_free();
        }

        else if _body.is_in_group("BoucArgent"){
            self.bouc = 1;
            godot_print!("Hello, world! How it going ? {}",self.bouc);
            bouc_argent.show();
            _body.queue_free();
        }
        else if _body.is_in_group("BoucOr"){
            self.bouc = 2;
            bouc_argent.hide();
            bouc_or.show();
            _body.queue_free();
        }
        
    }

}

use crate::extensions::NodeExt;
use gdnative::api::{KinematicBody2D, Node2D};
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(KinematicBody2D)]
#[user_data(user_data::MutexData<BulletIce>)]

/// Structure de la classe BulletIce.
pub struct BulletIce {

    velocity: Vector2,
}

/// Implémentation de la classe BulletIce.
#[methods]
impl BulletIce {

    fn new(_owner: &KinematicBody2D) -> Self {
        godot_print!("BulletIce new");
        BulletIce {

            velocity: Vector2::new(1200.0, 0.0),
        }

    }

    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup),
    /// et qui peut utiliser des fonctions qui simulent des évenments physiques.
    /// On fait avancer le bullet à la vitesse de velocity,
    /// donc de 1200 pixel par seconde en x et de 0 pixels par seconde en y
    #[export]
    fn _physics_process(&mut self, owner: &KinematicBody2D, delta: f64){
    
        owner.move_and_slide(self.velocity,Vector2::new(0.0, -1.0),true,2000,1.0,true);

    }

    /// Fonction qui permet de detecter lorsque le bullet sort du screen, ici on le supprime, il ne sert plus à rien.
    #[export]
    fn _on_VisibilityNotifier2D_screen_exited(&mut self,owner: &KinematicBody2D){
        owner.queue_free();
    }

    /// Cette fonction détecte les body2D qui entrent dans sa CollisionShape,
    /// On test si le body est dans le group "mob", on supprime le mob, puis on supprime le bullet.
    #[export]
    fn _on_Area2D_body_entered(&mut self,owner: &KinematicBody2D,_body: Variant ){

        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()
        };

        
        if _body.is_in_group("mobs") || _body.is_in_group("boss"){
            _body.queue_free();
            owner.queue_free();
        }
    }

}
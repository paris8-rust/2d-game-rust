use crate::extensions::NodeExt;
use gdnative::api::{KinematicBody2D, AnimatedSprite, Node2D};
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(KinematicBody2D)]
#[user_data(user_data::MutexData<Mob>)]

/// Structure de la classe Mob
pub struct Mob {

    #[property(default = 1000.0)]
    pub GRAVITY: f32,
    #[property(default = 200.0)]
    pub WALK_SPEED: f32,
    #[property(default = 400.0)]
    pub JUMP_SPEED: f32,

    velocity: Vector2,

    bouger: i32,

    droiteOuGauche: f32,

    player: Vector2,

}
/// Implémentation de la classe Mob.
#[methods]
impl Mob {

    /// On crée une nouvelle instance en initialisant toutes les variable.
    fn new(_owner: &KinematicBody2D) -> Self {

        Mob {
            GRAVITY:1000.0,
            WALK_SPEED:200.0,
            JUMP_SPEED:400.0,
            velocity:  Vector2::new(0.0, 0.0),
            bouger: 0,
            droiteOuGauche: 0.0,
            player: Vector2::new(0.0, 0.0),
        }
    }

    /// Cette fonction fonctionne avec un timer,
    /// chaque fois que le timer a effectué une boucle de x seconde (ici 1 seconde)
    /// On appelle cette fonction
    /// On passe à la variable bouger la valeur 1, qui indique le mob va bouger.
    /// Il bouge donc toute les 1 seconde.
    #[export]
    fn _on_Timer_timeout(&mut self,owner: &KinematicBody2D)  {
       self.bouger = 1;
    }

    /// Cette fonction détecte les body2D qui entrent dans sa CollisionShape,
    /// Si le body est dans le groupe player, on prend la position global du player,
    /// puis on démare le timer, qui va lui permettre de bouger.
    /// 
    #[export]
    fn _on_Area2D2_body_entered(&mut self,owner: &KinematicBody2D, _body: Variant) {

        let timer = unsafe { owner.get_typed_node::<Timer, _>("Timer") };
        let animation = "default";

        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()};

        if _body.is_in_group("player") {

            self.player = _body.global_position();
            timer.start(0.0);

        }
    }

    /// Cette fonction détecte les body2D qui sortent de sa CollisionShape,
    /// Si le body est dans le groupe player, on prend la position global du player,
    /// On passe à la variable bouger 0, il ne bougera plus.
    #[export]
    fn _on_AreaChaseEnemies_body_exited(&mut self,owner: &KinematicBody2D, _body: Variant) {

        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()};

        if _body.is_in_group("player") {
            self.bouger = 0;
        }
    }
    
    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup),
    /// et qui peut utiliser des fonctions qui simulent des évenments physiques.
    /// On va tester,
    /// si le mob est sur le sol: alors il peut bouger,
    /// si bouger est à 1, il peut bouger,
    /// si Les coordonnée du player sont à droite, le mob ira à droite,
    /// si les coordonnée du player sont à gauche, le mob ira à gauche.
    #[export]
    fn _physics_process(&mut self,owner: &KinematicBody2D,delta: f32){
        self.velocity.y = self.velocity.y + delta * self.GRAVITY;
        
        if owner.is_on_floor() 
        {
            if self.bouger == 1 {

                if self.player.x > owner.global_position().x {
                    self.droiteOuGauche = 1.0;
                    self.velocity.y = -self.JUMP_SPEED;
                    self.velocity.x = self.WALK_SPEED * self.droiteOuGauche;
                }
                else if self.player.x < owner.global_position().x {
                    self.droiteOuGauche = -1.0;
                    self.velocity.y = -self.JUMP_SPEED;
                    self.velocity.x = self.WALK_SPEED * self.droiteOuGauche;
                }
            }
        }   
        owner.move_and_slide(self.velocity,Vector2::new(0.0, -1.0),false,1,1.0,false);
    }
}


use crate::extensions::NodeExt;
use gdnative::api::{Label,StaticBody2D, Node2D,Area2D};
use gdnative::prelude::*;
use gdnative::*;

#[derive(NativeClass)]
#[inherit(StaticBody2D)]
#[user_data(user_data::MutexData<Boss>)]
#[register_with(Self::register_Boss)]
/// Structure de la classe Boss
pub struct Boss{

    vie: i32,

}
/// Implémentation de la classe Boss
#[methods]
impl Boss{

    /// Fonction permetant de gerer le signal "bossdead" qui sera utiliser dans la scene HUD.
    fn register_Boss(builder: &ClassBuilder<Self>){
        builder.add_signal(Signal {
            name: "bossdead",
            args: &[],

        });
    }

    /// On crée une nouvelle instance en initialisant toutes la variable.
    fn new(_owner: &StaticBody2D) -> Self {

        Boss {
            vie: 100,
        }

    }   

    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup)
    /// Ici on va initialiser la texte de la vie du boss, avec la variable vie, à 100.
    /// Qui va diminué à force qu'on tir sur le boss.
    #[export]
    fn _process(&mut self,owner: &StaticBody2D,delta: f32){
        let vie_boss = unsafe { owner.get_typed_node::<Label, _>("vieboss") };

        let string = self.vie.to_string();
        vie_boss.set_text(string);

    }

    /// Cette fonction détecte les area2D qui entrent dans sa CollisionShape,
    /// Ici on va tester si le body appartient au group "bulletice" ou au group "bulletfire"
    /// Si c'est un bulletice il prendra 1 point de dégat.
    /// Si c'est un bulletfire il prendra 5 points de dégats.
    /// Puis on test si le boss a 0 point de vie, si oui on émet le signal "bossdead" à Main.
    /// 
    /// 
    /// (Je n'ai pas trouver le moyen de les faire disparaitre,
    /// car les Area2D sont des childs des snodes BullerIce et BulletFIre,
    /// donc en jeu, les bullets passeront à travers, en lui faisant des dégats.
    /// Les queue_free() qu'on voit, font disparaitre les Area2D mais pas les nodes BulletFire et BulletIce)
    #[export]
    fn _on_Area2D_area_entered(&mut self,owner: &StaticBody2D,_body: Variant){

        let _body = unsafe { 
            _body.try_to_object::<Area2D>().expect("Failed to convert _body variant to Node2D").assume_safe()
        };

        godot_print!("Vie : {}",self.vie);
        
        if _body.is_in_group("bulletice"){
            self.vie = self.vie - 1;
            _body.queue_free();

        }
        else if _body.is_in_group("bulletfire"){
            self.vie = self.vie - 5;

            _body.queue_free();
        }

        if self.vie <= 0 {
            owner.emit_signal("bossdead", &[]);

            owner.queue_free();
        }
    }
}

use gdnative::prelude::*;

mod extensions;
mod Mob;
mod FlyingMob;
mod Player;
mod BulletIce;
mod BulletFire;
mod Main;
mod HUD;
mod Boss;
mod MobSpawner;


fn init(handle: InitHandle) {

    handle.add_class::<Mob::Mob>();
    handle.add_class::<FlyingMob::FlyingMob>();
    handle.add_class::<Player::Player>();
    handle.add_class::<BulletIce::BulletIce>();
    handle.add_class::<BulletFire::BulletFire>();
    handle.add_class::<Main::Main>();
    handle.add_class::<HUD::HUD>();
    handle.add_class::<Boss::Boss>();
    handle.add_class::<MobSpawner::MobSpawner>();
}

godot_init!(init);


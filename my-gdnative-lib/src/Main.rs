use crate::extensions::NodeExt;
use gdnative::api::{KinematicBody2D, Node};
use gdnative::prelude::*;
use gdnative::*;

#[derive(NativeClass)]
#[inherit(Node)]
#[user_data(user_data::MutexData<Main>)]

/// Structure de la classe Main
pub struct Main{

    bosstue: i32,
}
/// Implémentation de la classe Main
#[methods]
impl Main{

    /// On crée une nouvelle instance en initialisant la variable.
    fn new(_owner: &Node) -> Self {

        Main {
            bosstue: 0,
        }
    }

    /// Fonction qui est appellée une seule fois, quand le body2D est instancié.
    /// On cache le player.
    #[export]
    fn _ready(&mut self, owner: &Node){
        let player = unsafe { owner.get_typed_node::<KinematicBody2D, _>("Player") };

        player.show();
        
    }

    /// Fonction qui est appellé lorsqu'on appuie sur un bouton,
    /// Ici c'est le bouton start du node HUD, donc quand on appuie dessus, on affiche le player.
    #[export]
    fn _on_HUD_start_game(&mut self, owner: &Node){
        let player = unsafe { owner.get_typed_node::<KinematicBody2D, _>("Player") };

        player.show();
    }


}
use crate::extensions::NodeExt;
use gdnative::api::{Node2D,AnimatedSprite,Timer};
use gdnative::prelude::*;
use gdnative::*;
use crate::Mob;
#[derive(NativeClass)]
#[inherit(Node2D)]
#[user_data(user_data::MutexData<MobSpawner>)]


/// Structure de la classe MobSpawner, Avec une référence sur la classe Mob.
/// Ca servira à instancier des mob à partir du mob spawner.
pub struct MobSpawner{
    #[property]
    Mob: Option<Ref<PackedScene,Shared>>,
    #[property(default = 10)]
    nbSpawn: i64,
    time: i64,
    montre: i64,
    childrenfc: i64,

}
/// Implémentation de la classe MobSpawner,
#[methods]
impl MobSpawner{

    /// On crée une nouvelle instance en initialisant toutes les variable.
    fn new(_owner: &Node2D) -> Self {

        MobSpawner {

            Mob: None,
            nbSpawn: 9,
            time: 1,
            montre: 0,
            childrenfc: 0, 

        }
    }

    /// Fonction qui est appellée une seule fois, quand le body2D est instancié.
    /// Ici on cache le mob au joueur, et on importe la scene Mob.
    #[export]
    fn _ready(&mut self, owner: &Node2D){

        owner.hide();

        let resource_loader = ResourceLoader::godot_singleton();

        self.Mob = resource_loader
            .load("res://Mob.tscn", "PackedScene", false)
            .and_then(|res| res.cast::<PackedScene>());
    }
    /// Fonction qui est éxecuter un certain nombre de fois par secondes(beaucoup)
    /// Ici on va vérifié quand est ce que le mobspawner sera free,
    /// On test si tout les mobs ont bien été construit, puis on free le mobSpawner.
    #[export]
    fn _process(&mut self, owner: &Node2D,delta: f32){

        if self.time > self.nbSpawn {
            owner.queue_free();
        }
    }

    /// Cette fonction fonctionne avec un timer,
    /// chaque fois que le timer a effectué une boucle de x seconde (ici 1 seconde)
    /// On appelle cette fonction
    /// Cette fonction va créér une instance de la scene Mob,
    /// à la position du owner, donc de MobSpawner, dans le noeud principal.MobSpawner.
    /// Puis on ajoute à time 1, pour compter combien de mob on a crée,
    /// C'est cette variable qu'on testera dans le fonction _process.
    #[export]
    fn _on_MobSpawn_timeout(&mut self, owner: &Node2D){

            if let Some(mob_scene) = self.Mob.take() {
                let mob_scene = unsafe { mob_scene.assume_safe() };
                let mob = mob_scene
                    .instance(PackedScene::GEN_EDIT_STATE_DISABLED)
                    .and_then(|b| {
                        let b = unsafe { b.assume_unique() };
                        b.cast::<KinematicBody2D>()
                    })
                    .expect("Could not intantiate mob scene!");

                mob.set_global_position(Vector2::new(owner.global_position().x,owner.global_position().y));

                if let Some(root_scene) = owner.get_node("/root/Main"){
                    let root_scene = unsafe {root_scene.assume_safe()};
                    root_scene.add_child(mob,false);
                }

                self.Mob.replace(mob_scene.claim());
            }

            
        self.time = self.time + 1;
    }

    /// Cette fonction détecte les body2D qui entrent dans sa CollisionShape,
    /// Vous pourrez allez voir dans godot,
    /// Ici on prend le _body qui est entré dans la CollisionShape du MobSpawner,
    /// Si ce body est du type "player" , on rend le MobSpawner visible au player.
    /// Puis on démarre le timer, et on lance l'animation du MobSpawner.
    #[export]
    fn _on_Area2D_body_entered(&mut self, owner: &Node2D,_body: Variant){

        let _body = unsafe { 
            _body.try_to_object::<Node2D>().expect("Failed to convert _body variant to Node2D").assume_safe()
        };

        let mob_spawn = unsafe { owner.get_typed_node::<Timer, _>("MobSpawn") };

        
        if _body.is_in_group("player") {
        
            owner.show();
            mob_spawn.start(0.0);
            let sprite = unsafe { owner.get_typed_node::<AnimatedSprite, _>("Sprite") };
            let animation = "default";
            sprite.play("default",false);
            
        }
            

    }

}
use crate::extensions::NodeExt;
use gdnative::api::{Label,Button,CanvasLayer};
use gdnative::prelude::*;
use gdnative::*;

#[derive(NativeClass)]
#[inherit(CanvasLayer)]
#[user_data(user_data::MutexData<HUD>)]
#[register_with(Self::register_HUD)]
/// Structure de la classe HUD (vide)
pub struct HUD{

}
/// Implémentation de la classe HUD.
#[methods]
impl HUD{

    /// Fonction permetant de gerer le signal "bossdead" qui sera utiliser dans la scene Main.
    fn register_HUD(builder: &ClassBuilder<Self>){
        builder.add_signal(Signal {
            name: "start_game",
            args: &[],

        });
    }   

/// On crée une nouvelle instance
    fn new(_owner: &CanvasLayer) -> Self {

        HUD {}
    }

    /// Fonction qui est appellée une seule fois, quand le body2D est instancié.
    #[export]
    fn _ready(&mut self, owner: &CanvasLayer){
        
        let game_over = unsafe { owner.get_typed_node::<Label, _>("GameOver") };
        let win = unsafe { owner.get_typed_node::<Label, _>("Win") };
        game_over.hide();
        win.hide();
    }

    /// Fonction qui est appellé lorsqu'on appuie sur un bouton,
    /// Lorsque l'utilisateur appuie, il est caché, et on émet le signal "start_game" à main.
    #[export]
    fn _on_StartButton_pressed(&mut self, owner: &CanvasLayer){

        let start_button = unsafe { owner.get_typed_node::<Button, _>("StartButton") };
        start_button.hide();
        owner.emit_signal("start_game",&[]);

    }

    /// Fonciton qui reçoit le signal de player, lorsqu'il meurt, On affiche le message de fin : GameOver
    #[export]
    fn _on_Player_dead(&mut self, owner: &CanvasLayer){

        let game_over = unsafe { owner.get_typed_node::<Label, _>("GameOver") };
        game_over.show();


    }

    /// Fonction qui reùoit le signal de Boss, lorsqu'il meurt on affiche le message de fin : Congratulation, You win !
    #[export]
    fn _on_Boss_bossdead(&mut self, owner: &CanvasLayer){

        let win = unsafe { owner.get_typed_node::<Label, _>("Win") };
        win.show();

    }


}
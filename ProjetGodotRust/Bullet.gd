extends Area2D

var speedBullet = 700  # How fast the player will move (pixels/sec).

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _physics_process(delta):
	position += transform.x * speedBullet * delta



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

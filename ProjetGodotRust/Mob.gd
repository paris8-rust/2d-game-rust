extends KinematicBody2D

export var GRAVITY = 1000
export var WALK_SPEED = 200
export var JUMP_SPEED = 300
var player = 0
var velocity = Vector2()
var bouger = 0
var droiteOuGauche = 0


func _physics_process(delta):
	velocity.y += delta * GRAVITY
	
	if is_on_floor():
		velocity.y = GRAVITY/5
		
		if player:
			if (player.position.x - global_position.x) < 0:
				droiteOuGauche = -1
			else:
				droiteOuGauche = 1
			if bouger == 1:
				velocity.y = -JUMP_SPEED
				velocity.x = droiteOuGauche * WALK_SPEED
		else:
			velocity.x = 0
			
				
				
	bouger = 0
		
	move_and_slide(velocity, Vector2(0, -1))
		
# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()
	$MobASprite.play()


func _on_Timer_timeout():
	bouger = 1
		

func _on_Area2D2_body_entered(body):
	if body.is_in_group("player"):
		player = body
	
func _on_AreaChaseEnemies_body_exited(body):
	if body.is_in_group("player"):
		player = 0


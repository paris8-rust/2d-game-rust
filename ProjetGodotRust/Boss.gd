extends StaticBody2D

signal bossdead

export var vie = 100;


func _process(delta):
	$vieboss.text = str(vie);
	if vie <= 0:
		emit_signal("bossdead")
		queue_free()
		
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area2D_area_entered(area):
	if area.is_in_group("bulletice"):
		vie = vie -1
		area.get_parent().queue_free()
	if area.is_in_group("bulletfire"):
		vie = vie -5
		area.get_parent().queue_free()

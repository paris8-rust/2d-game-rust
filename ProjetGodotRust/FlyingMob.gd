extends KinematicBody2D

export (int) var speed = 200


var velocity = Vector2()
var player = 0
var target = Vector2()

func _ready():
	$AnimatedSprite.play()

func _physics_process(delta):
	if player:
		target = player.position
		
	velocity = position.direction_to(target) * speed
	velocity = move_and_slide(velocity)

func _on_Detection_body_entered(body):
	if body.is_in_group("player"):
		player = body
	target = position


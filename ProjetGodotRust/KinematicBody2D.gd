extends KinematicBody2D

export (PackedScene) var BulletIce
export (PackedScene) var BulletFire
export var GRAVITY = 2000
export var WALK_SPEED = 500
export var JUMP_SPEED = 700
export var en_vie = true;
export var vie = 3

signal dead

var dire = Vector2()
var velocity = Vector2()

var nbsautenlair = 0
var bouc = 0
var Arme = 1



func _ready():
	$coeur3.show()
	$coeur2.show()
	$coeur.show()
	$BoucArgent.hide()
	$BoucOr.hide()

func _process(delta):
	if $TimerFireBullet.get_time_left() == 0:
		$Fire.hide()
	if en_vie:
		if Input.is_action_just_pressed("shoot") and Arme == 1:
			shootIce()
		if Input.is_action_just_pressed("shoot") and Arme == 2 and $TimerFireBullet.get_time_left() == 0:
			shootFire()
		if Input.is_action_just_pressed("Ice"):
			Arme = 1
		if Input.is_action_just_pressed("Fire"):
			Arme = 2
	

	
	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	
	dire = get_global_mouse_position() - global_position
	
	if is_on_floor():
		nbsautenlair = 0
		velocity.y = GRAVITY/10

	if Input.is_action_pressed("ui_left"):
		velocity.x = -WALK_SPEED
		$PlayerAnimation.animation = "walk"
		$PlayerAnimation.flip_h = true
		$PlayerAnimation.play()
		
	elif Input.is_action_pressed("ui_right"):
		velocity.x =  WALK_SPEED
		$PlayerAnimation.animation = "walk"
		$PlayerAnimation.flip_h = false
		$PlayerAnimation.play()
	else:
		velocity.x = 0
		$PlayerAnimation.stop()
		$PlayerAnimation.animation = "stoped"
		$PlayerAnimation.play()

	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		velocity.y = -JUMP_SPEED

	if not is_on_floor():
		if Input.is_action_pressed("ui_left"):
			velocity.x = -WALK_SPEED/1.5
		elif Input.is_action_pressed("ui_right"):
			velocity.x =  WALK_SPEED/1.5
		else:
			velocity.x = 0
			$PlayerAnimation.stop()
			$PlayerAnimation.animation = "stoped"
			$PlayerAnimation.play()
		
	if not is_on_floor() and Input.is_action_just_pressed("ui_up") and nbsautenlair == 0:
		nbsautenlair = 1
		velocity.y = -JUMP_SPEED
		

	affichage_vie()

	move_and_slide(velocity, Vector2(0, -1))

func _on_Area2D_body_entered(body):
	if body.is_in_group("mobs") or body.is_in_group("boss"):
		hit()
		$PlayerAnimation.animation = "hit"
		$PlayerAnimation.play()
		if vie == 0:
			en_vie = false
			emit_signal("dead")
			hide()
			$CollisionShape2D.disabled
			GRAVITY = 0
			WALK_SPEED = 0
			JUMP_SPEED = 0
	if body.is_in_group("ItemCoeur"):
		if vie != 3:
			vie = vie +1
		body.queue_free()
	if body.is_in_group("BoucArgent"):
		bouc = 1
		$BoucArgent.show()
		body.queue_free()
	if body.is_in_group("BoucOr"):
		$BoucOr.show()
		$BoucArgent.hide()
		body.queue_free()
		bouc = 2
		
func shootIce():
	var aa = BulletIce.instance()
	aa.start($pdd.global_position, dire.angle())
	owner.add_child(aa)
	
func shootFire():
	$TimerFireBullet.start()
	$Fire.show()
	var aa = BulletFire.instance()
	aa.start($pdd.global_position, dire.angle())
	owner.add_child(aa)

func hit():
	if bouc == 0:
		$BoucArgent.hide()
		vie = vie - 1
	if bouc == 1:
		$BoucArgent.hide()
		bouc = 0
	if bouc == 2:
		$BoucArgent.show()
		$BoucOr.hide()
		bouc = 1

func affichage_vie():
	if vie == 3:
		$coeur3.show()
	elif vie == 2:
		$coeur3.hide()
		$coeur2.show()
	elif vie == 1:
		$coeur2.hide()
		

extends Node

export (PackedScene) var Player
export var bosstue = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.hide()
	$Player.WALK_SPEED =0
	$Player.JUMP_SPEED =0
	$Player.en_vie =false

func _on_HUD_start_game():
	$Player.show()
	$Player.WALK_SPEED =500
	$Player.JUMP_SPEED =700
	$Player.en_vie =true

extends Node2D

export (PackedScene) var Mob
export (PackedScene) var Player

export var nbSpawn= 5
var time = 1
var montre = 0
var nbNewChildren = 0
var ChildDepart = 0
var childrenfc = 0

# Called when the node enters the scene tree for the first time.
func _ready():	
	hide()


func _process(delta):
	if time > nbSpawn and get_child_count() == ChildDepart:
		print(childrenfc)
		queue_free()
	
	
func _on_MobSpawn_timeout():
	var mob = Mob.instance()
	mob.position = $Spawn.position
	add_child(mob)
	nbNewChildren = nbNewChildren +1
	time = time + 1
	$Sprite.play()
	if time > nbSpawn:
		$MobSpawn.stop()
	
func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		montre =montre + 1
		if montre == 1:
			ChildDepart = get_child_count()
			show()
			$MobSpawn.start()
	

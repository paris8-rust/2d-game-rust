extends CanvasLayer

signal start_game

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (PackedScene) var Player

# Called when the node enters the scene tree for the first time.
func _ready():
	$GameOver.hide()
	$Win.hide()



func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")


func _on_Player_dead():
	$GameOver.show()


func _on_Boss_bossdead():
	$Win.show()

extends Area2D

export var speed = 750

# How fast the player will move (pixels/sec).

# Called when the node enters the scene tree for the first time.

func _physics_process(delta):
	position += transform.x * speed * delta


func _on_Bullet_body_entered(body):
	if body.is_in_group("mobs"):
		body.queue_free()
		queue_free()
	if body.is_in_group("wall"):
		queue_free()
